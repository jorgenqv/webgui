from flask import Flask, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
import yaml
from wtforms import validators
from flask_admin.form import rules
from sqlalchemy import or_

fp = open('webgui.conf', 'r')
conf = yaml.load(fp)

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{}:{}@{}/{}'.format(conf['db_user'], conf['db_pass'], conf['server'], conf['database'])
app.config['SECRET_KEY'] = 'we are borg'


db = SQLAlchemy(app)
admin = Admin(app, name='DB ADMIN', template_mode='bootstrap3')


class bib(db.Model):
    rowid = db.Column(db.Integer, primary_key=True)
    customerid = db.Column(db.Integer, nullable='FALSE')
    msisdnid = db.Column(db.Integer)
    actiondate = db.Column(db.DateTime)
    amount = db.Column(db.Integer)
    used_top = db.Column(db.String(45))
    service = db.Column(db.String(45))
    used_cons = db.Column(db.String(45))


class distributors(db.Model):
    distributorid = db.Column(db.Integer, primary_key=True)
    companyname = db.Column(db.String(45))
    province = db.Column(db.String(45))


class msisdn(db.Model):
    msisdnid = db.Column(db.Integer, primary_key=True)
    customerid = db.Column(db.Integer)
    distributorid = db.Column(db.Integer)
    retailerid = db.Column(db.Integer)
    activationdate = db.Column(db.DateTime)
    used_act = db.Column(db.String(45))


class retailers(db.Model):
    retailerid = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(45))
    lastname = db.Column(db.String(45))


class conscomplevels(db.Model):
    amountfrom = db.Column(db.Float)
    amountto = db.Column(db.Float)
    compamount = db.Column(db.Integer)
    comppercent = db.Column(db.Float)
    row_id = db.Column(db.Integer, primary_key=True)


class customers(db.Model):
    customerid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(45))
    address = db.Column(db.String(45))


class outputcomm(db.Model):
    distributorid = db.Column(db.Integer)
    retailerid = db.Column(db.Integer)
    msisdnid = db.Column(db.Integer)
    calculationdate = db.Column(db.DateTime)
    amount = db.Column(db.Float)
    commtype = db.Column(db.String(45))
    row_id = db.Column(db.Integer, primary_key=True)


class topactcomplevels(db.Model):
    amountfrom = db.Column(db.Float)
    amountto = db.Column(db.Float)
    compamount = db.Column(db.Integer)
    comppercent = db.Column(db.Float)
    row_id = db.Column(db.Integer, primary_key=True)


class bibView(ModelView):
    can_create = False
    can_edit = False
    can_delete = False
    column_display_pk = True


class customersView(ModelView):
    can_create = False
    can_edit = False
    can_delete = False
    column_display_pk = True


class msisdnView(ModelView):
    can_create = False
    can_edit = False
    can_delete = False
    column_display_pk = True


class distributorsView(ModelView):
    column_display_pk = True
    form_columns = ['distributorid', 'companyname', 'province']

    form_args = {
        'companyname': {
            'label': 'Companyname',
            'validators': [validators.required()]
        },
        'province': {
            'label': 'Province',
            'validators': [validators.required()]
        },
        'distributorid': {
            'label': 'Distributorid',
            'validators': [validators.required(), validators.number_range(100100, 100999, 'Distributorid out of range!')]
        }
    }

    form_edit_rules = [
        rules.Field('companyname'),
        rules.Field('province')
        ]

    def on_model_delete(self, model):
        if msisdn.query.filter_by(distributorid=model.distributorid).first() is not None:
            raise ValueError('Distributor have one or more records in msisdn!')

        if outputcomm.query.filter_by(distributorid=model.distributorid).first() is not None:
            raise ValueError('Distributor have one or more records in outputcomm!')


class retailersView(ModelView):
    column_display_pk = True
    form_columns = ['retailerid', 'firstname', 'lastname']
    form_args = {
        'firstname': {
            'label': 'Firstname',
            'validators': [validators.required()]
        },
        'lastname': {
            'label': 'Lastname',
            'validators': [validators.required()]
        },
        'retailerid': {
            'label': 'Retailerid',
            'validators': [validators.required(), validators.number_range(1001, 9999 , 'Retailerid out of range!')]
        }
    }

    form_edit_rules = [
        rules.Field('firstname'),
        rules.Field('lastname')
    ]

    def on_model_delete(self, model):
        if msisdn.query.filter_by(retailerid=model.retailerid).first() is not None:
            raise ValueError('Retailer have one or more records in msisdn!')

        if outputcomm.query.filter_by(retailerid=model.retailerid).first() is not None:
            raise ValueError('Retailer have one or more records in outputcomm!')


class outputcommView(ModelView):
    can_create = False
    can_edit = False
    can_delete = False


class conscomplevelsView(ModelView):
    def on_model_change(self, form, model, is_created):
        if model.amountfrom >= model.amountto:
            raise ValueError('Amountfrom from is greater than or equal to Amountto')

        try:
            conscomplevels.query.filter(or_(conscomplevels.amountfrom == model.amountfrom, conscomplevels.amountto == model.amountfrom)).one()
            conscomplevels.query.filter(or_(conscomplevels.amountfrom == model.amountto, conscomplevels.amountto == model.amountto)).one()
            conscomplevels.query.filter(or_(conscomplevels.amountfrom.between(model.amountfrom, model.amountto), conscomplevels.amountto.between(model.amountfrom, model.amountto))).one()
        except:
            raise ValueError('Overlapping values..')

class topactcomplevelsView(ModelView):
    def on_model_change(self, form, model, is_created):
        if model.amountfrom >= model.amountto:
            raise ValueError('Amountfrom from is greater than or equal to Amountto')

        try:
            topactcomplevels.query.filter(or_(topactcomplevels.amountfrom == model.amountfrom, topactcomplevels.amountto == model.amountfrom)).one()
            topactcomplevels.query.filter(or_(topactcomplevels.amountfrom == model.amountto, topactcomplevels.amountto == model.amountto)).one()
            topactcomplevels.query.filter(or_(topactcomplevels.amountfrom.between(model.amountfrom, model.amountto), topactcomplevels.amountto.between(model.amountfrom, model.amountto))).one()
        except:
            raise ValueError('Overlapping values..')


# Some redirect help if user can not find admin url to avoid ugly 404 error message
# Hint from https://stackoverflow.com/questions/14343812/redirecting-to-url-in-flask

@app.route("/")
def index():
    return redirect('/admin')

@app.route("/admin")
def redirected():
    return "admin"


# Add administrative views here
admin.add_view(bibView(bib, db.session))
admin.add_view(conscomplevelsView(conscomplevels, db.session))
admin.add_view(msisdnView(msisdn, db.session))
admin.add_view(outputcommView(outputcomm, db.session))
admin.add_view(distributorsView(distributors, db.session))
admin.add_view(retailersView(retailers, db.session))
admin.add_view(customersView(customers, db.session))
admin.add_view(topactcomplevelsView(topactcomplevels, db.session))

app.run(host='0.0.0.0', port=int(conf['port']))
